# makefile for applications based on my1imgpro

EXECUTE = my1video_app
OBJECTS = my1video_app.o my1video_work.o my1video_csim.o
EXTOBJS = my1image.o my1video.o
EX2OBJS = my1list.o my1text.o my1path.o my1thread.o
OBJECTS += $(EXTOBJS) $(EX2OBJS)

EXTPATH = ../my1imgpro/src
EX2PATH = ../my1codelib/src
CFLAG += -Wall -I$(EXTPATH) -I$(EX2PATH)

VFLAG = -DMY1APP_VERS=\"$(shell date +%Y%m%d)\"
TFLAG += $(shell pkg-config --cflags gtk+-3.0)
CFLAG += $(VFLAG) $(TFLAG)
GFLAG += $(shell pkg-config --libs gtk+-3.0)
OFLAG += $(GFLAG) -lavcodec -lavutil -lavformat -lswscale -lavdevice
LFLAG += $(OFLAG) -lm -lpthread

RM = rm -f
CC = gcc -c
LD = gcc

debug: CFLAG += -g -DMY1_DEBUG

all: $(EXECUTE)

new: clean all

debug: new

${EXECUTE}: $(OBJECTS)
	$(LD) $(CFLAG) -o $@ $+ $(LFLAG)

%.o: src/%.c src/%.h
	$(CC) $(CFLAG) -o $@ $<

%.o: src/%.c
	$(CC) $(CFLAG) -o $@ $<

%.o: $(EXTPATH)/%.c $(EXTPATH)/%.h
	$(CC) $(CFLAG) $(DFLAG) -o $@ $<

%.o: $(EX2PATH)/%.c $(EX2PATH)/%.h
	$(CC) $(CFLAG) -o $@ $<

clean:
	-$(RM) $(EXECUTE) *.o
