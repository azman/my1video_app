/*----------------------------------------------------------------------------*/
/*
	my1video_csim => my1video_app conveyor simulator library
	Author: azman@my1matrix.net
*/
/*----------------------------------------------------------------------------*/
#ifndef __MY1VIDEO_CSIMH__
#define __MY1VIDEO_CSIMH__
/*----------------------------------------------------------------------------*/
#include "my1image_util.h"
#include "my1list.h"
#include "my1path.h"
#include "my1thread.h"
/*----------------------------------------------------------------------------*/
typedef struct _my1video_csim_t
{
	my1image_t *wait, *next, *show, view;
	my1image_buffer_t work;
	my1list_t full;
	my1path path;
	my1thread_t loader;
	int maxrow, maxcol;
	int donext; /* sliding index in next image */
	int dostep; /* sliding step size */
}
my1video_csim_t;
/*----------------------------------------------------------------------------*/
typedef my1video_csim_t my1vcsim_t;
/*----------------------------------------------------------------------------*/
void video_csim_init(my1video_csim_t* csim);
void video_csim_free(my1video_csim_t* csim);
void video_csim_next(my1video_csim_t* csim);
void video_csim_path(my1video_csim_t* csim, char* path);
/*----------------------------------------------------------------------------*/
#endif
/*----------------------------------------------------------------------------*/
