/*----------------------------------------------------------------------------*/
/*
	my1video_work => my1video_app work library
	Author: azman@my1matrix.org
*/
/*----------------------------------------------------------------------------*/
#ifndef __MY1VIDEO_WORKH__
#define __MY1VIDEO_WORKH__
/*----------------------------------------------------------------------------*/
#include "my1video_main.h"
#include "my1video_csim.h"
/*----------------------------------------------------------------------------*/
typedef struct _my1video_work_t
{
	my1video_main_t vmain;
	my1video_csim_t vcsim;
	int vtype, vflag;
	char *vname;
	/* useful pointers here :p */
	my1video_t *video;
	my1vgrab_t *vgrab;
	my1vappw_t *vappw;
}
my1video_work_t;
/*----------------------------------------------------------------------------*/
#define VWORK_FLAG_OK 0
#define VWORK_FLAG_ERROR_PARAM 0x01
/*----------------------------------------------------------------------------*/
typedef my1video_work_t my1vwork_t;
/*----------------------------------------------------------------------------*/
void video_work_args(my1vwork_t* vwork, int argc, char *argv[]);
void video_work_init(my1vwork_t* vwork);
void video_work_free(my1vwork_t* vwork);
void video_work_reboot(my1vwork_t* vwork);
void video_work_reload(my1vwork_t* vwork, char* filename);
void video_work_nograb(my1vwork_t* vwork, char* pathname);
/*----------------------------------------------------------------------------*/
#endif
/*----------------------------------------------------------------------------*/
