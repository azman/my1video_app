/*----------------------------------------------------------------------------*/
/*
	my1video_csim => my1video_app conveyor simulator library
	Author: azman@my1matrix.net
*/
/*----------------------------------------------------------------------------*/
#include <stdio.h>
#include <stdlib.h>
/*----------------------------------------------------------------------------*/
#include "my1video_csim.h"
#include "my1image_crgb.h"
#include "my1image_file.h"
/*----------------------------------------------------------------------------*/
#define MAX_WIDTH 640
#define MAX_HEIGHT 480
/*----------------------------------------------------------------------------*/
void video_csim_load(my1video_csim_t* csim, my1image_t* that)
{
	char *name;
	my1image_buffer_t *buff = &csim->work;
	my1image_t* temp = &buff->xtra;
	/* should ALWAYS get an image file from list - reset if done */
	if (!list_iterate(&csim->full))
		csim->full.curr = csim->full.init;
	name = (char*) csim->full.curr->item;
	/* load dummy if failed to load actual file */
	if(image_load(temp,name)<0)
	{
		image_make(that,csim->maxrow,csim->maxcol);
		image_fill(that,BLACK);
	}
	else image_size_this(temp,that,csim->maxrow,csim->maxcol);
}
/*----------------------------------------------------------------------------*/
void* video_csim_load_thread(void *arg)
{
	my1video_csim_t* csim = (my1video_csim_t*) arg;
	my1thread_t* code = &csim->loader;
	code->running = 1;
	while (!code->stopped)
	{
		if (!csim->wait)
		{
			video_csim_load(csim,csim->work.next);
			csim->wait = csim->work.next;
		}
	}
	code->running = 0;
	thread_done(code);
	return 0x0;
}
/*----------------------------------------------------------------------------*/
void video_csim_init(my1video_csim_t* csim)
{
	csim->wait = 0x0;
	csim->next = 0x0;
	csim->show = &csim->view;
	image_init(&csim->view);
	buffer_init(&csim->work);
	list_setup(&csim->full,LIST_TYPE_DEFAULT);
	path_init(&csim->path);
	thread_init(&csim->loader,video_csim_load_thread);
	csim->maxrow = MAX_HEIGHT;
	csim->maxcol = MAX_WIDTH;
	csim->donext = 0;
	csim->dostep = 10; /* default is 1 */
}
/*----------------------------------------------------------------------------*/
void video_csim_free(my1video_csim_t* csim)
{
	image_free(&csim->view);
	buffer_free(&csim->work);
	list_clean(&csim->full,list_free_item);
	path_free(&csim->path);
	if (csim->loader.running)
	{
		csim->wait = &csim->view; /* disable dummy wait */
		csim->loader.stopped = 1; /* stop loader thread */
		while (csim->loader.running);
		thread_wait(&csim->loader);
	}
	thread_free(&csim->loader);
}
/*----------------------------------------------------------------------------*/
void video_csim_next(my1video_csim_t* csim)
{
	int irow, icol, rows, cols, tabs;
	int ichk, inxt, step, stop;
	int *prow, *nrow;
	rows = csim->show->rows;
	cols = csim->show->cols;
	tabs = csim->next->cols;
	step = csim->dostep;
	stop = cols - step;
	ichk = csim->donext;
	prow = csim->show->data;
	/* start sliding, by row */
	for (irow=0;irow<rows;irow++)
	{
		/* slide step pixels to left */
		for (icol=0;icol<stop;icol++)
			prow[icol] = prow[icol+step];
		prow += cols;
	}
	/* get the rest from incoming - get column by column? */
	for (icol=stop,inxt=ichk;icol<cols;icol++,inxt++)
	{
		if (inxt==tabs)
		{
			/* need next image in wait! */
			while (!csim->wait);
			buffer_swap(&csim->work);
			csim->next = csim->work.curr;
			csim->wait = 0x0;
			inxt = 0;
			nrow = csim->next->data;
			tabs = csim->next->cols;
		}
		prow = &csim->show->data[icol];
		nrow = &csim->next->data[inxt];
		for (irow=0;irow<rows;irow++)
		{
			prow[0] = nrow[0];
			prow += cols;
			nrow += tabs;
		}
	}
	csim->donext = inxt;
}
/*----------------------------------------------------------------------------*/
void video_csim_path(my1video_csim_t* csim, char* path)
{
	my1image_t that;
	my1list temp;
	path_access(&csim->path,path);
	if (csim->path.flag<0)
	{
		printf("Invalid path '%s'?\n",path);
		exit(-1);
	}
	list_setup(&temp,LIST_TYPE_DEFAULT);
	path_dolist(&csim->path,&temp,PATH_LIST_FILE);
	image_init(&that);
	while (temp.count>0)
	{
		char *name = (char*) list_pull_item(&temp,0x0);

		if(image_load(&that,name)<0)
		{
			free((void*)name);
			continue;
		}
		/* transfer to actual list */
		list_push_item(&csim->full,(void*)name);
	}
	image_free(&that);
	list_clean(&temp,list_free_item);
	/* should have number of supported image file(s) */
	if (csim->full.count<2)
	{
		printf("Not enough resources in '%s'! (%d)\n",
			csim->path.full,csim->full.count);
		exit(-1);
	}
	/* prepare to browse list */
	csim->full.curr = 0x0;
	csim->donext = 0; /* index - next */
	/** prepare buffer */
	buffer_size_all(&csim->work,csim->maxrow,csim->maxcol);
	/** use blank as first frame */
	image_make(csim->show,csim->maxrow,csim->maxcol);
	image_fill(csim->show,BLACK);
	csim->show->mask = IMASK_COLOR;
	csim->wait = 0x0;
	/* manually load first image */
	video_csim_load(csim,csim->work.curr);
	csim->next = csim->work.curr;
	/* start loader thread - wait until first load */
	thread_exec(&csim->loader,(void*)csim);
}
/*----------------------------------------------------------------------------*/
