/*----------------------------------------------------------------------------*/
/*
	my1video_work => my1video_app work library
	Author: azman@my1matrix.net
*/
/*----------------------------------------------------------------------------*/
#include "my1video_work.h"
#include "my1text.h"
/*----------------------------------------------------------------------------*/
#include <stdlib.h>
#include <string.h>
/*----------------------------------------------------------------------------*/
void video_work_args(my1vwork_t* vwork, int argc, char *argv[])
{
	int loop;
	/* initialize these */
	vwork->vtype = VIDEO_SOURCE_NONE;
	vwork->vflag = VWORK_FLAG_OK;
	vwork->vname = 0x0;
	/* check parameter */
	for (loop=1;loop<argc;loop++)
	{
		if (argv[loop][0]!='-')
		{
			vwork->vname = argv[++loop];
			vwork->vtype = VIDEO_SOURCE_FILE;
			break;
		}
		else if (!strcmp(argv[loop],"--path"))
		{
			vwork->vname = argv[++loop];
			break;
		}
		else if (!strcmp(argv[loop],"--live"))
		{
			/* on linux this should be /dev/video0 or something... */
			vwork->vname = argv[++loop];
			vwork->vtype = VIDEO_SOURCE_LIVE;
			break;
		}
		else vwork->vflag |= VWORK_FLAG_ERROR_PARAM;
	}
	/* setup filters - video_main_init loads work filter */
	for (++loop;loop<argc;loop++)
		video_main_pass_load(&vwork->vmain,argv[loop]);
}
/*----------------------------------------------------------------------------*/
void video_work_init(my1vwork_t* vwork)
{
	/* initialize stuffs */
	video_main_init(&vwork->vmain);
	/* alternative grabber */
	video_csim_init(&vwork->vcsim);
	/* assign pointers */
	vwork->video = &vwork->vmain.video;
	vwork->vgrab = &vwork->vmain.vgrab;
	vwork->vappw = &vwork->vmain.vappw;
}
/*----------------------------------------------------------------------------*/
void video_work_free(my1vwork_t* vwork)
{
	/* cleanup csim structure */
	video_csim_free(&vwork->vcsim);
	/* cleanup main structure */
	video_main_free(&vwork->vmain);
}
/*----------------------------------------------------------------------------*/
void video_work_reboot(my1vwork_t* vwork)
{
	my1vmain_t *vmain = &vwork->vmain;
	video_main_pass_done(vmain);
}
/*----------------------------------------------------------------------------*/
void video_work_reload(my1vwork_t* vwork, char* filename)
{
	my1text text;
	my1vmain_t *vmain = &vwork->vmain;
	text_init(&text);
	text_open(&text,filename);
	if (text.pfile)
	{
		my1str_token pfind;
		strtok_init(&pfind,COMMON_DELIM);
		/* clear all filters */
		video_work_reboot(vwork);
		/* 1 filter per line */
		while (text_read(&text)>=CHAR_INIT)
		{
			strtok_prep(&pfind,&text.pbuff);
			if (strtok_next(&pfind))
			{
				/* check if commented line */
				if (pfind.tstep==1&&pfind.token.buff[0]=='#')
					continue;
				/* try to insert */
				video_main_pass_load(vmain,pfind.token.buff);
				/* ignore the rest */
			}
		}
		strtok_free(&pfind);
		text_done(&text);
	}
	text_free(&text);
}
/*----------------------------------------------------------------------------*/
void video_csim_grab(void* grabdata)
{
	my1video_work_t* vwork = (my1video_work_t*)grabdata;
	my1video_csim_t* vcsim = (my1video_csim_t*)&vwork->vcsim;
	/* reset new frame flag and check if update required */
	vwork->video->flags &= ~VIDEO_FLAG_NEW_FRAME;
	if (!(vwork->video->flags&VIDEO_FLAG_DO_UPDATE)) return;
	video_csim_next(vcsim);
	vwork->video->frame = vcsim->show;
	vwork->video->flags |= VIDEO_FLAG_NEW_FRAME;
	if (vwork->video->flags&VIDEO_FLAG_STEP)
		vwork->video->flags &= ~VIDEO_FLAG_DO_UPDATE;
}
/*----------------------------------------------------------------------------*/
void video_work_nograb(my1vwork_t* vwork, char* pathname)
{
	/* ignore if already with source */
	if (vwork->vmain.type!=VIDEO_SOURCE_NONE)
		return;
	video_csim_path(&vwork->vcsim,pathname);
	/* will NOT get here if unsuccessful load */
	vwork->video->frame = vwork->vcsim.show;
	/* assign function pointers */
	vwork->vmain.grabber = video_csim_grab;
	vwork->vmain.grabber_data = (void*)vwork;
	/* configure like live grab */
	vwork->video->index = -1;
	vwork->video->flags |= VIDEO_FLAG_DO_UPDATE;
	vwork->video->flags &= ~VIDEO_FLAG_STEP;
}
/*----------------------------------------------------------------------------*/
