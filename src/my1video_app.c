/*----------------------------------------------------------------------------*/
#include "my1video_work.h"
/*----------------------------------------------------------------------------*/
#include <stdio.h>
#include <string.h>
/*----------------------------------------------------------------------------*/
#define MY1APP_NAME "my1video_app"
#define MY1APP_INFO "Video Tool"
/* MY1APP_VERS can be set in makefile */
#ifndef MY1APP_VERS
#define MY1APP_VERS "build"
#endif
/*----------------------------------------------------------------------------*/
int main(int argc, char* argv[])
{
	my1image_t *view;
	my1vwork_t vwork;
	/* print tool info */
	printf("\n%s - %s (%s)\n",MY1APP_NAME,MY1APP_INFO,MY1APP_VERS);
	printf("  => by azman@my1matrix.org\n\n");
	/* initialize app */
	video_work_init(&vwork);
	/* check args */
	video_work_args(&vwork,argc,argv);
	if (vwork.vflag)
	{
		printf("-- Request error (%d)!\n",vwork.vflag);
		return -vwork.vflag;
	}
	/* check video source */
	if (!vwork.vname)
	{
		printf("-- No video source requested!\n");
		exit(-1);
	}
	/* setup capture */
	video_main_capture(&vwork.vmain,vwork.vname,vwork.vtype);
	/* alternative grabber */
	if (vwork.vmain.type==VIDEO_SOURCE_NONE)
	{
		printf("Loading... ");
		fflush(stdout);
	}
	video_work_nograb(&vwork,vwork.vname);
	if (vwork.vmain.type==VIDEO_SOURCE_NONE)
	{
		printf("done.(%s:%d)\n\n",
			vwork.vcsim.path.full,vwork.vcsim.full.count);
		fflush(stdout);
	}
	view = vwork.video->frame;
	if (!view->size)
	{
		printf("-- Zero-length image!\n");
		exit(-1);
	}
	/* initialize gui */
	gtk_init(&argc,&argv);
	/* setup display */
	video_main_display(&vwork.vmain,"MY1Video Tool");
	/* prepare menu & events */
	video_main_prepare(&vwork.vmain);
	/* tell them */
	printf("Starting main capture loop.\n\n");
	fflush(stdout);
	/* setup display/capture cycle */
	video_main_loop(&vwork.vmain,VGRAB_DELAY);
	/* main loop */
	gtk_main();
	/* clean up */
	video_work_free(&vwork);
	/* done */
	printf("\n");
	return 0;
}
/*----------------------------------------------------------------------------*/
